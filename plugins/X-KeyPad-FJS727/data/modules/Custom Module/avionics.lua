-- avionics.lua - Used to test stuff without breaking other modules.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         08/15/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:
--
-- Thsi module has the following subcomponents.
--
--  transponder

--=============================================================================
-- Simulator datarefs and commands 
-- Simulator Datarefs

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs, shared ints, shared floats, and custom strings
-- Datarefs

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables

--=============================================================================
-- Components

components = {
    transponder {},
}

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize() - Synchronize the X-Keypad and simulator variables.

function initialize()

    logInfo("Initializing avionics.")


end -- End function initialize

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize()

--------------------------------------------------------------------------------
-- Update
function update()
    updateAll(components)
end -- End function update
