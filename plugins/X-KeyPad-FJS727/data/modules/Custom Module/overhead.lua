-- overhead.lua - Processes Overhead panel
--
-- This module process the Overhead panel for the pilots.  It calls
-- various modules to do the work.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         11/29/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:
--
-- This module has the following subcomponents.
--  lights - Processes the lights on the Overhead panel

--=============================================================================
-- Simulator Datarefs

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables

--=============================================================================
-- Components
components = {
    lights {}
}

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if this is being called on startup, false if called by
--  any other routine.
--
-- Returns:
--  None

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing overhead.")
    end


end -- End function initialize

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

    updateAll(components)

end -- End function update
