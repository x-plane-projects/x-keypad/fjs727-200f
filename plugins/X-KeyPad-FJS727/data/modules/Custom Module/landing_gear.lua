-- landing_gear.lua - Handles landing gear systems.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         01/05/2021    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Simulator Datarefs
local gear_handle_lever = globalProperty("FJS/727/Hyd/GearHandleMo")

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints
-- See main.lua
--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables
local gear_lever_position = 16   -- Lua index - X-Plane  + 1
-- Values for gear handle in down, off, up positions.
local geardown_value = 0
local gearoff_value = 1
local gearup_value = 2

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if this is being called on startup, false if called by
--  any other routine.
--
-- Returns:
--  None

function initialize(firstcall)

    local gearhandlepos

    if firstcall then
        logInfo("Initializing landing_gear.")
    end

    -- Set the X-KeyPad value to the simulator value.
    gearhandlepos = get(gear_handle_lever)
    set(shared_ints, gearhandlepos, gear_lever_position)

end -- End function initialize

--------------------------------------------------------------------------------
-- gear_handle() - Handles synchronizing the X-KeyPad value with the simulator
-- and vice versa.
--
-- Params:
--  None
--
-- Returns:
--  None

function gear_handle()
    local gearhandlepos
    gearhandlepos = get(gear_handle_lever)

    -- The position is reported as a float which doesn't always get to
    -- the values of 0, 1, or 2 exactly so we have to convert it to an
    -- integer.
    -- If the value is < .5 the handle is down
    --print("Gear handle pos:"..gearhandlepos)
    if gearhandlepos < 0.5 then
        set(shared_ints, geardown_value, gear_lever_position)
        return 0
    end
    -- If the value is >= 0.5 and <= 1.5 then it's off.
    if (gearhandlepos >= 0.5) and (gearhandlepos <= 1.5) then
        set(shared_ints, gearoff_value, gear_lever_position)
        return
    end
    -- If it's > 1.5 then it's up.
    if gearhandlepos > 1.5 then
        set(shared_ints, gearup_value, gear_lever_position)
        return 0
    end

end -- End function gear_handle
--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()
    gear_handle()
end -- End function update
