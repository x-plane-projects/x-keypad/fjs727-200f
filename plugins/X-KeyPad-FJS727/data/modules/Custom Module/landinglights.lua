-- landinglights.lua - Processes landing lights.
--
-- Initializes the all landing light switch to off.  The other landing light
-- switches are left alone as the pilot may have them on from a previous save.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         12/08/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Simulator Datarefs

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables
local offvalue = 0

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if this is being called on startup, false if called by
--  any other routine.
--
-- Returns:
--  None

function initialize(firstcall)

    local allldgltindex = 4
    local allldgltsw

    if firstcall then
        logInfo("Initializing landinglights.")
    end

    -- Set All landing light switch to off. This is an X-KeyPad key that does
    -- not exist in the simulator but was added to make life easier.  The All
    -- landing light switch turns all landing lights on or off.
    set(shared_ints, offvalue, allldgltindex)


end -- End function initialize

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

end -- End function update
