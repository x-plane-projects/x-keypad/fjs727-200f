-- lights.lua - Handles lighting on the Overhead panel.
--
-- A subcomponent of overhead that handles lighting on the
-- overhead panel.  Lighting is controlled by rheostats that have
-- a range of 0 to 1.  The X-KeyPad csv file can not handle this
-- range so for each rheostat a shared int is set to 0-100 and
-- this value is converted to a 0-1 range and written to the dataref.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         12/01/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:
-- This module has the following subcomponents.
--  landinglights - Processes the landing lights.

--=============================================================================
-- Simulator Datarefs
local lights_mapknobL = globalProperty("FJS/727/lights/MapLiteL_Knob")
local domelight_sw = globalProperty("FJS/727/lights/DomeLightSwitch")

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables

-- Variables for dimmer processing
local dimmer_info

--=============================================================================
-- Components
components = {
    landinglights {}
}

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if this is being called on startup, false if called by
--  any other routine.
--
-- Returns:
--  None

function initialize(firstcall)

    local datarefprop
    local simvalue
    local divisor

    -- On the first call send message to log and get dimmer information setup.
    if firstcall then
        logInfo("Initializing overhead/lights.")

        -- Set up dimmer/light control
        dimmer_info_init()

    end

    -- Dimmers
    -- Sync X-KeyPad values to the simulator.
    -- The last simulator and X-KeyPad values in dimmer_info are updated also.
    -- Dimmers.  Process each one in turn.
    for dimmer, dimmerinfo in ipairs(dimmer_info) do
        datarefprop = globalProperty(dimmerinfo[4])
        simvalue = get(datarefprop)
        divisor = dimmerinfo[3]
        dimmerinfo[6] = math.floor(simvalue * divisor)
        set(shared_ints, math.floor(simvalue * divisor), dimmerinfo[1])
    end

end -- End function initialize

--------------------------------------------------------------------------------
-- dimmer_info_init() - initializes the data for the dimmers.
-- Dimmers (or light controls) are used to control panel lighting and other features
-- that require a variable level and in real life are normally controlled by a rehostat.
-- In the simulator can have their ranges expressed in various ways
-- such as from 0.0-1.0 as a float, or 0-100 as an integer and there can be
-- many dimmers to process.  In order to avoid coding for each dimmer that might exist
-- and to minimize the use of shared integers as well as handling the various
-- formats the following system is used.
--
-- Each dimmer is defined in the dimmer_info variable that is initialized by this
-- function.  This function is called on the first run of this module.
-- Each entry contains the following information.
--
--  index - The shared integer index Lua 1 based.
--  name - A name for the dimmer.  Not used but makes it easier to remember what each
--          dimmer entry is for.
--  divisor - Scales the shared int which is an integer to a value for the simulator dataref.
--      The X-KeyPad csv file can only handle integer ranges such as 0-100 and this is what
--      is stored in the shared int.  However, the simulator may use other ranges such as
--      0.0-1.0 so the divisor is used to convert the shared int value to a value in the
--      range used by the simulator and vice versa.
--  datarefstr - A string with the dataref associated with this dimmer.
--  lastsimpos - The last simulator value for this dimmer.
--  lastxkeypad - The last X-Keypad value for this dimmer.
--
-- When this module is called for the first time the initialization routine runs this function
-- to populate the dimmer information and set the X-KeyPad dimmer values to the same as the
-- simulator.  After that the updates processes each dimmer checking to see if the simulator
-- or X-KeyPad value has changed and updating the other value.
--
-- Parameters:
--  None
--
-- Returns:
--  None.  The module's local variable dimmer_info is set with the values for each dimmer.

function dimmer_info_init()


    -- Setup the dimmer information.
    dimmer_info = {
        {
            5,
            "Map Light Left",
            100,
            "FJS/727/lights/MapLiteL_Knob",
            .15,
            11
        };
        {
            6,
            "Left FWD & Side Panel",
            100,
            "FJS/727/lights/Panel_LeftFwd_Knob",
            .25,
            21
        };
        {
            7,
            "Overhead Panel",
            100,
            "FJS/727/lights/Panel_CtrFwd_Knob",
            0,
            0
        };
        {
            8,
            "FWD Panel Background",
            100,
            "FJS/727/lights/Fwd_Panel_storm_Knob",
            0,
            0
        };
        {
            9,
            "Overhead Panel",
            100,
            "FJS/727/lights/Panel_Overhead_Knob",
            0,
            0,
        };
        {
            10,
            "Control Stand - Red",
            100,
            "FJS/727/lights/CtrStnd_Red_Knob",
            0,
            0
        };
        {
            11,
            "Control Stand - White",
            100,
            "FJS/727/lights/WhiteCtrlStndKnob",
            0,
            0
        };
        {
            12,
            "Right Forward & Side Panel",
            100,
            "FJS/727/lights/Panel_RightFwd_Knob",
            0,
            0
        };
        {
            13,
            "Left Map Light",
            100,
            "FJS/727/lights/MapLiteR_Knob",
            0,
            0,
        };
        {
            14,
            "Compass",
            100,
            "FJS/727/lights/Panel_Compass_Knob",
            0,
            0
        };
        {
            15,
            "Dome Red",
            100,
            "FJS/727/lights/DomeRed_Knob",
            0,
            0
        }
    }

end -- End function dimmer_info_init

--------------------------------------------------------------------------------
-- dimmers() - Controls the lighting dimmers on the overhead panel.
-- Each dimmer in the dimmer_info variable is checked to see if the simulator or
-- X-KeyPad value has changed.  If one has changed the other is updated.
--
-- Parameters:
--  None.
--
-- Returns:
--  None

function dimmers()

    local currxkeypad
    local simpos
    local datarefprop
    local lastsimvalue
    local lastxkeypadvalue
    local divisor

    -- Process each dimmer in turn.
    for dimmer, dimmerinfo in ipairs(dimmer_info) do
    --    -- Get the current simulator and X-KeyPad values
    --    -- Simpos is in the simulator range, not the shared int range.
        datarefprop = globalProperty(dimmerinfo[4])
        simpos = get(datarefprop)
        currxkeypad = get(shared_ints, dimmerinfo[1])
        lastsimvalue = dimmerinfo[5]
        lastxkeypadvalue = dimmerinfo[6]
        divisor = dimmerinfo[3]

        -- if the simulator value has changed update the X-KeyPad value.
        if (simpos ~= lastsimvalue) then
            set(shared_ints, math.floor(simpos * divisor), dimmerinfo[1])
            dimmerinfo[5] = simpos
            dimmerinfo[6] = math.floor(simpos * divisor)
            lastxkeypadvalue = dimmerinfo[6]
            currxkeypad = dimmerinfo[6]
        end

        -- If the X-KeyPad value has changed update the simulator value.
        if (currxkeypad ~= lastxkeypadvalue) then
            set(datarefprop, rheostat(currxkeypad, divisor))
            dimmerinfo[5] = get(datarefprop)
            dimmerinfo[6] = currxkeypad
        end
    end
end -- End function dimmers

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

    dimmers()
end -- End function update
