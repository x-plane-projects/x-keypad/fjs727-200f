-- transponder.lua - Handles transponder functions.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         08/15/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Simulator datarefs and commands 
-- Simulator Datarefs
sim_txpdrmode = globalProperty("sim/cockpit2/radios/actuators/transponder_mode")

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs, shared ints, shared floats, and custom strings
-- Datarefs

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings
custom_string_00 = globalProperty("SRS/X-KeyPad/custom_string_0")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables
-- Transponder mode 0=Off, 1=Standby, 2=On, 3=Altitude, 4=Test
local txpdr_mode
local txpdr_modestr= {"OFF",
    "STDBY",
    "ON",
    "ALT",
    "TEST"
}

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize() - Synchronize the X-Keypad and simulator variables.

function initialize()

    logInfo("Initializing transponder")

    -- Set X-KeyPad custom string to current mode.
    txpdr_mode = get(sim_txpdrmode)
    set(custom_string_00, txpdr_modestr[txpdr_mode + 1])

end -- End function initialize

--------------------------------------------------------------------------------
-- Set mode string - set string value of mode

function set_modestr()
    -- Set X-KeyPad custom string to current mode.
    txpdr_mode = get(sim_txpdrmode)
    set(custom_string_00, txpdr_modestr[txpdr_mode + 1])

end -- End function set_modestr

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize()

--------------------------------------------------------------------------------
-- Update
function update()

    set_modestr()

end -- End function update
