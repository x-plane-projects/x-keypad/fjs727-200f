-- hydraulics.lua - This and it's sub components handle the
-- systems FJS lists under FJS/727/Hyd
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         01/05/2021    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:
--
-- This module has the following subcomponents
--      landing_gear

--=============================================================================
-- Simulator Datarefs

--------------------------------------------------------------------------------
-- Simulator Commands

--=============================================================================
-- X-KeyPad datarefs used

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables

--=============================================================================
-- Components
components = {
    landing_gear {}
}
--=============================================================================
-- Components definition.

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if this is being called on startup, false if called by
--  any other routine.
--
-- Returns:
--  None

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing Hydraulics.")
    end

end -- End function initialize

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()
    updateAll(components)
end -- End function update
