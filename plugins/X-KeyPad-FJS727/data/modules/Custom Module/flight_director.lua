-- flight_director.lua - Handles the flight director functions.
--
-- Longer description of what this does.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         09/05/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:

--=============================================================================
-- Simulator Datarefs

--  Module(s)           Dataref
fd_modeselector = globalProperty("FJS/727/autopilot/FDModeSelector")
--------------------------------------------------------------------------------
-- Simulator Commands

--	Module(s)			Commands
fd_modeselectleft = sasl.findCommand("FJS/727/Autopilot/FD_SelectLeft")
fd_modeselectright = sasl.findCommand("FJS/727/Autopilot/FD_SelectRight")

--=============================================================================
-- X-KeyPad datarefs used

--	Module(s)			Dataref

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--  Module(s)           String             Usage
custom_string_01 = globalProperty("SRS/X-KeyPad/custom_string_1")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--  Module(s)           Int Index       Usage

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--  Module(s)           Float Index       Usage

--=============================================================================
-- Local variables
local fdmodelastsim
local fdmodelastxkeypad

local fdmodeselectknob_index = 3     -- Lua index starting at 1 (X-Plane 0 index)

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize() - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  fd_called - true if the autopilot on routine called us, otherwise false.
--
-- Returns:
--  None

function initialize(firstcall)

    if (firstcall) then
        logInfo("Initializing ...")
    end

    -- Sync X-KeyPad Flight Director mode to the simulator.
    lastsim = get(fd_modeselector)
    lastxkeypad = lastsim
    -- Set X-KeyPad Shared Int datarefs
    set(shared_ints, lastsim, fdmodeselectknob_index)

    -- Set custom string to ON or off.
    if (fd_ison()) then
        set_custstring(custom_string_01, "ON")
    else
        set_custstring(custom_string_01, "OFF")
    end

end -- End function initialize

--------------------------------------------------------------------------------
-- fdmode_knob_control() - Handles the flight director mode selector knob.  The
-- simulator and X-KeyPad values are passed to the knob control routine along
-- with the properties containing the commands and shared int index.
--
-- Parameters:
--  None
--
-- Returns:
--  None

function fdmode_knob_control()

    local currxkeypad
    local simpos

    -- Set custom string to ON or off.
    if (fd_ison()) then
        set_custstring(custom_string_01, "ON")
    else
        set_custstring(custom_string_01, "OFF")
    end

    -- If the simulator value has changed update the shared int.
    simpos = get(fd_modeselector)
    currxkeypad = get(shared_ints, fdmodeselectknob_index)
    if(simpos ~= fdmodelastsim) then
        set(shared_ints, simpos, fdmodeselectknob_index)
        fdmodelastsim = simpos
        fdmodelastxkeypad = simpos
        currxkeypad = simpos
    end

    -- If the shared int value has changed (a button was pressed) process it.
    if (fdmodelastxkeypad ~= currxkeypad) then
        change_knob(currxkeypad, simpos, fd_modeselectright, fd_modeselectleft)
        pitchlastxkeypad = currxkeypad
        pitchlastsim = get(fd_modeselector)
    end

end -- End function pitch_knob_control

--------------------------------------------------------------------------------
-- fd_ison()() - Returns true if the flight director is on, otherwise false.
-- This is based on the dataref ap_select.
--
-- Parameters:
--  None
--
-- Returns:
--  true if the flight director is on, otherwise false

function fd_ison()

    if (get(fd_modeselector) == 0) then
        return false
    else
        return true
    end

end -- End function fd_ison

----------------------------------------------------------------------------------
-- set_custstring(customstr, thevalue) - Sets custom string to value.
--
-- Parameters
--  customstr - the property containing the custom string dataref
--  thevalue - the value to set customstr to

function set_custstring(customstr, thevalue)

    set(customstr, thevalue)

end -- End function set_custstring

--=============================================================================
--Processing functions
-- Initialization done once on entry.
initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

    fdmode_knob_control()

end -- End function update
