-- autopilot.lua - Autopilot processing (not the flight director).
--
-- Process the autopilot and not the flight director which is in another module.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         08/29/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:
--
--  1.  Pitch select knob - This controls the mode the autopilot is in which
--      can be pitch hold, vertical speed hold, IAS hold, or MACH hold.  The
--      pitchselectknob dataref has values from -2 to 1 to indicate the mode it
--      is in.  However, the position is controlled by PitchSelectLeft and
--      PitchSelectRight commands in the sim so just setting the dataref
--      does not control the setting.  The command has to be executed to move
--      the knob to the desired position.
--
--      To control the pitch select knob from both X-KeyPad and the simulator
--      will require synchronization between the two.  Control from the simulator
--      and reflecting the status in X-KeyPad is easy enough since it simply
--      requires X-KeyPad showing the status of the simulator on the X-KeyPad
--      buttons.
--      Control of the pitch select knob from X-KeyPad is done by selecting
--      the desired button or pressing the pitch select left/right buttons
--      until the desired function is active.
--
--      X-KeyPad control requires that the simulator commands be used to move
--      the pitch selector knob to the desired setting which will update the
--      datarefs allowing X-KeyPad to reflect the setting of the pitch selector
--      knob.  This is done using a Shared_Int that takes the value of the
--      pitch select knob dataref.  When the module starts X-KeyPad variables
--      are initialized to reflect the simulator values.
--
--      When the X-KeyPad pitch select left or right button is pressed this
--      script issues the pitch knob commands to move the knob in the proper
--      direction.  The Shared_Int is updated to the value for the new position.
--      If one of the indicator buttons such as IAS hold is pressed the
--      Shared_Int is set to the value for that position and this script them
--      moves the pitch select knob in the proper direction to the new
--      position.
--
--      The PitchSelectKnob dataref has values of
--          -2 Mach Hold
--          -1 IAS Hold
--           0 Vert Speed Hold
--           1 Pitch Hold
--
--  2. Nav Selector knob - this works in the same way as the pitch selector knob.
--
--      The NavSelectorKnob dataref has values of
--          -2 Aux Nav
--          -1 Nav Loc
--           0 Turn knob
--           1 Auto G/S
--           2 Man G/S

--=============================================================================
-- Simulator datarefs and commands 
-- Simulator Datarefs
ap_lever = globalProperty("FJS/727/autopilot/AP_Lever")
ap_pitchselectknob = globalProperty("FJS/727/autopilot/PitchSelectKnob")
ap_navselectknob = globalProperty("FJS/727/autopilot/NavSelectKnob")
ap_lever = globalProperty("FJS/727/autopilot/AP_Lever")
ap_navgps = globalProperty("FJS/727/NAVGPSSwitch")

--------------------------------------------------------------------------------
-- Simulator Commands
ap_pitchselectleft = sasl.findCommand("FJS/727/autopilot/PitchSelectLeft")
ap_pitchselectright = sasl.findCommand("FJS/727/autopilot/PitchSelectRight")
ap_navselectleft = sasl.findCommand("FJS/727/autopilot/NavSelectLeft")
ap_navselectright = sasl.findCommand("FJS/727/autopilot/NavSelectRight")
ap_select =  sasl.findCommand("FJS/727/Autopilot/AP_Select")

--=============================================================================
-- X-KeyPad datarefs, shared ints, shared floats, and custom strings
-- Datarefs

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings
custom_string_02 = globalProperty("SRS/X-KeyPad/custom_string_2")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--=============================================================================
-- Local variables

-- These store the last values for the sim and X-Keypad and are used to
-- determine who made the change and update appropriately.
local pitchlastsim
local pitchlastxkeypad
local navlastsim
local pitchlastxkeypad
local sw_navgps

local pitchselectknob_index = 1     -- Lua index starting at 1 (X-Plane 0 index)
local navselectknob_index = 2       -- Lua index starting at 1 (X-Plane 0 index)

--=============================================================================
-- Components

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- initialize(firstcall) - Synchronize the X-Keypad and simulator variables.
--
-- Parameters:
--  firstcall - true if the first call when the script starts. Mainly to print
--  the startup message only when the script is started.
--
-- Returns:
--  None

function initialize(firstcall)

    local thestring

    -- If this is the first pass on startup log it, otherwise don't.
    if (firstcall) then
        logInfo("Initializing autopilot.")
    end

    -- Sync X-KeyPad pitch knob setting to the simulator.
    lastsim = get(ap_pitchselectknob)
    lastxkeypad = lastsim
    -- Set X-KeyPad Shared Int datarefs
    set(shared_ints, lastsim, pitchselectknob_index)

    -- Sync X-KeyPad nav selector knob setting to the simulator.
    lastsim = get(ap_navselectknob)
    lastxkeypad = lastsim
    -- Set X-KeyPad Shared Int datarefs
    set(shared_ints, lastsim, navselectknob_index)

    -- Sync X-KeyPad NAV/GPS switch to the simulator.
    -- Set the custom string to indicate what is in use.
    sw_navgps = get(ap_navgps)
    if (sw_navgps == 1) then
        thestring = string.format(">NAV\nGPS")
        set_custstring(custom_string_02, thestring)
    else
        thestring = string.format("NAV\n>GPS")
        set_custstring(custom_string_02, thestring)
    end

end -- End function initialize

--------------------------------------------------------------------------------
-- pitch_knob_control() - Handles the pitch selector knob on the autopilot.  The
-- simulator and X-KeyPad values are passed to the knob control routine along
-- with the properties containing the commands and shared int index.
--
-- Parameters:
--  None
--
-- Returns:
--  None

function pitch_knob_control()

    local currxkeypad
    local simpos

    -- if the autopilot is not on skip this.
    if (not ap_ison()) then
        return
    end

    -- If the simulator value has changed update the shared int.
    simpos = get(ap_pitchselectknob)
    currxkeypad = get(shared_ints, pitchselectknob_index)
    if(simpos ~= pitchlastsim) then
        set(shared_ints, simpos, pitchselectknob_index)
        pitchlastsim = simpos
        pitchlastxkeypad = simpos
        currxkeypad = simpos
    end

    -- If the shared int value has changed (a button was pressed) process it.
    if (pitchlastxkeypad ~= currxkeypad) then
        change_knob(currxkeypad, simpos, ap_pitchselectright, ap_pitchselectleft)
        pitchlastxkeypad = currxkeypad
        pitchlastsim = get(ap_pitchselectknob)
    end

end -- End function pitch_knob_control

--------------------------------------------------------------------------------
-- navselector_knob_control() - Handles the nav selector knob on the autopilot.  The
-- simulator and X-KeyPad values are passed to the knob control routine along
-- with the properties containing the commands and shared int index.
--
-- Parameters:
--  None
--
-- Returns:
--  None

function navselector_knob_control()

    local currxkeypad
    local simpos

    -- if the autopilot is not on skip this.
    if (not ap_ison()) then
        return
    end

    -- If the simulator value has changed update the shared int.
    simpos = get(ap_navselectknob)
    currxkeypad = get(shared_ints, navselectknob_index)
    if(simpos ~= navlastsim) then
        set(shared_ints, simpos, navselectknob_index)
        navlastsim = simpos
        navlastxkeypad = simpos
        currxkeypad = simpos
    end

    -- If the shared int value has changed (a button was pressed) process it.
    if (navlastxkeypad ~= currxkeypad) then
        change_knob(currxkeypad, simpos, ap_navselectright, ap_navselectleft)
        navlastxkeypad = currxkeypad
        navlastsim = get(ap_navselectknob)
    end

end -- End function pitch_knob_control

--------------------------------------------------------------------------------
-- nav_gps() - Handles the position of the NAV/GPS switch. Sets up a custom
-- string with the current selection indicated.  This is used because the csv
-- can't do it.
--
-- Parameters:
--  None
--
-- Returns:
--  None

function nav_gps()

    sw_navgps = get(ap_navgps)
    if (sw_navgps == 1) then
        thestring = "->NAV\nGPS"
        set_custstring(custom_string_02, thestring)
    else
        thestring = "NAV\n->GPS"
        set_custstring(custom_string_02, thestring)
    end

end -- End function nav_gps
--------------------------------------------------------------------------------
-- ap_ison() - Returns true if autopilot is on, otherwise false.
-- This is based on the datarefe ap_select.
--
-- Parameters:
--  None
--
-- Returns:
--  true if the autopilot is on, otherwise false

function ap_ison()

    if (get(ap_lever) == 1) then
        return true
    else
        initialize(false)
        return false
    end
end -- End function ap_ison

--------------------------------------------------------------------------------
-- set_custstring(customstr, thevalue) - Sets custom string to value.
--
-- Parameters
--  customstr - the property containing the custom string dataref
--  thevalue - the value to set customstr to

function set_custstring(customstr, thevalue)

    set(customstr, thevalue)

end -- End function set_custstring

--=============================================================================
--Processing functions
-- Initialization done once on entry.

initialize(true)

--------------------------------------------------------------------------------
-- Update
function update()

    pitch_knob_control()
    navselector_knob_control()
    nav_gps()
end -- End function update
