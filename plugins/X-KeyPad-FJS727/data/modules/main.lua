-- main.lua - Script main().
--
-- Runs all the other modules.
--
-- Revisions:
--
-- Number       Date        Name/Comments
--  1.0         08/29/2020    Dakota Pilot
--                          Original version.

--=============================================================================
-- Notes:
--
-- These modules are for the FJS 727-200F and can be used for the 727-100 and
-- 727-200Adv at this point. That may change in the future.
-- These modules handle calucations and processing that the X-KeyPad csv file
-- cannot.
--
--=============================================================================
-- Datarefs and commands for all modules
-- These are documented here to keep the information in one place for easy
-- reference instead of having to search all the modules.
-- The indices here are X-Plane zero based.  Use Lua 1 based in the code so add
-- 1 to these values when they are indexed in Lua.
-- Simulator Datarefs

--  Module(s)           Dataref
--  autopilot           FJS/727/autopilot/PitchSelectKnob
--                      FJS/727/autopilot/NavSelectKnob
--                      FJS/727/NAVGPSSwitch
--  flight_director     FJS/727/autopilot/FDModeSelector
--  lights              FJS/727/lights/MapLiteL_Knob
--                      FJS/727/lights/DomeLightSwitch
--  transponder         sim/cockpit2/radios/actuators/transponder_mode

--------------------------------------------------------------------------------
-- Simulator Commands

--	Module(s)			Commands
--  autopilot           FJS/727/autopilot/PitchSelectLeft
--                      FJS/727/autopilot/PitchSelectRight
--                      FJS/727/autopilot/NavSelectLeft
--                      FJS/727/autopilot/NavSelectRight
--                      FJS/727/autopilot/AP_Lever
--                      FJS/727/Autopilot/AP_Select
--                      FJS/727/Hyd/Com/GearHandleTuggle (FJS spelling, not mine).

--=============================================================================
-- X-KeyPad datarefs used

--	Module(s)			Dataref

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--  Module(s)           String             Usage
--  transponder             0              Indicates transponder mode.
--  flight_director         1              Either ON or OFF to show FD state
--  autopilot               2              NAV/GPS string to indicate choice

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints
-- X-Plane index, zero based.  Add 1 for Lua based.
--
--  Module(s)           Int Index       Usage
--  autopilot                   0       Pitch selector knob position
--                                          -2 MACH Hold
--                                          -1 IAS Hold
--                                          0 Vert speed Hold
--                                          1 Pitch Hold
--                               1      Nav Selector knob position
--                                          -2 Aux Nav
--                                          -1 Nav Loc
--                                           0 Turn knob
--                                           1 Auto G/S
--                                           2 Man G/S
-- flight_director                2     Flight director mode knob position
--                                          -1 GA (Go Around)
--                                           0 OFF
--                                           1 HDG
--                                           2 NAV/LOC
--                                           3 APPR AUTO
--                                           4 APPR MAN
-- Overhead Panel                 3     Turn on  all landing lights
--                                           0 - All Off
--                                           1 - All On
-- The Light control shared ints are used to set various lights to 0 or 100%
-- to make it easier to get full on or full off.
-- Light controls                  4-14 Sets the corresponding light to 0 or 100%
--  brightness.
--
-- This shared int is used to make sure X-KeyPad displays the correct
-- value as it doesn't handle moving from 0 to 1 t0 2 properly and
-- grabs the first value after 0 instead of following the dataref.  Since
-- the dataref is a floating point value and doesn't always take the
-- exact value the cvs file needs the  value is converted to an integer
-- by this plugin.
-- Gear Handle Position           15     Landing gear position
--                                            0 - Down
--                                            1 - Off
--                                            2 - Up

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--  Module(s)           Float Index       Usage

--=============================================================================
-- These are datarefs, custom strings, shared ints/floats and commands actually
-- used by main or globally.
-- Simulator Datarefs

--	Module(s)			Dataref

--------------------------------------------------------------------------------
-- Simulator Commands

--	Module(s)			Command

--=============================================================================
-- X-KeyPad datarefs
-- Datarefs

--	Module(s)			Dataref
-- landing_gear         "FJS/727/Hyd/GearHandleMo"

--------------------------------------------------------------------------------
-- X-KeyPad Custom Strings

--  Module(s)           String             Usage
--  transponder              0             Transponder mode as string

--------------------------------------------------------------------------------
-- X-KeyPad Shared Ints

--  Module(s)           Int Index       Usage
shared_ints = globalProperty("SRS/X-KeyPad/SharedInt")

--------------------------------------------------------------------------------
-- X-KeyPad Shared Floats

--  Module(s)           Float Index       Usage

--=============================================================================
-- Components definition.
components = {
    autopilot {},
    avionics {},
    flight_director {},
    transponder {},
    overhead {},
    hydraulics {}
}
--=============================================================================
-- SASL Options
-- Turn off things we do not need
sasl.options.setAircraftPanelRendering(false)
sasl.options.set3DRendering(false)
sasl.options.setInteractivity(false)

--=============================================================================
-- Functions

--------------------------------------------------------------------------------
-- change_knob(newpos, oldpos, knobup, knobdown) - Moves a knob such as pitch selector or nav selector to the
-- desired position.
--
-- Parameters:
--  newpos - the new position we want to move simulator
--  oldpos - the old, current position
--  knobup - the property containing the command to turn the knob up/right
--  knobdown - the property containing the command to turn the knob down/left
--
-- Returns:
--  None

function change_knob(newpos, oldpos, knobup, knobdown)

    local diff
    local i

    -- Find out which way to go up/down right/left
    diff = newpos - oldpos
    -- Move the knob to the desired position.
    -- Move it, check the value and if it's the desired position quit
    -- otherwise move it again.
    -- Move it down (left, toward the pilot)
    if diff < 0 then
        for i = oldpos, newpos + 1, -1
        do
            sasl.commandOnce(knobdown)
        end
    end

    -- Move it up (right) toward the first officer
    if diff > 0 then
        for i = oldpos, newpos - 1, 1
        do
            sasl.commandOnce(knobup)
        end
    end

end -- End function change_knob

--------------------------------------------------------------------------------
-- rheostat(value, divisor) - used to control rheostat and potentiometer type controls
-- commonly used in lighting control.  The csv file can not handle values that
-- are not integers.  This function is here so it can be called by any module.
-- The value from the X-KeyPad dataref (such as a shared int) is divided by divisor
-- to produce a value scaled to fit the simulator dataref.
-- Most simulator lighting control datarefs run between 0.0 and 1.0 as floats.
--
-- Parameters:
--  value - the value from X-KeyPad dataref such as a shared integer.
--  divisor - the value that the input value is divided by to scale the value to
--  a range the simulator dataref requires.
--
-- Returns:
--  The value/divisor as a float.

function rheostat(value, divisor)

    return value/(divisor * 1.0) -- use 1.0 to force to float.

end -- End function rheostat

--------------------------------------------------------------------------------
-- initialize() - syncs X-KeyPad data with the simulator data.
--
-- Parmeters:
--  None
--
-- Returns:
--  None

function initialize(firstcall)

    if firstcall then
        logInfo("Initializing FJS 727-200Adv main.")
    end

    --newWindow = contextWindow {
    --    name = "Window Test",
    --    position = {50, 50, 600, 600},
    --    noBackground = false,
    --    visible = false,
    --    vrauto = true,
    --    components = {
    --        wMain {
    --            position = {0, 0, 512, 512, bg = {1, 1, 1, 0.5}}
    --        };
    --        textbox {
    --        };
    --    };
    --}

end -- End function initialize

--------------------------------------------------------------------------------

--=============================================================================
--Processing functions
initialize(true)
--------------------------------------------------------------------------------
-- Update

function update()
    updateAll(components)
end


